import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link, Redirect} from "react-router-dom";
import "react-big-calendar/lib/css/react-big-calendar.css";
import BigCalendar from 'react-big-calendar';
import moment from "moment";
import {getMeetupList} from '../../store/actions/meetupActions';

const localizer = BigCalendar.momentLocalizer(moment);

class Calendar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            events: []
        }
    }

    componentDidMount() {
        this.getPropsCalendar();
        this.setEventsState();
    }

    getPropsCalendar = ()=>{
        this.props.getMeetupList();
    }

    setEventsState = () => {
        const {meetupList} = this.props;
        meetupList.forEach((meetup) => {
            let event = {
                start: new Date(Date.parse(meetup.date)),
                end: new Date(Date.parse(meetup.date)),
                title: <Link to={'/meetup/' + meetup._id} className="valign-wrapper calendar-title"><i
                    className="material-icons prefix calendar-title-icon">arrow_forward</i>{meetup.info}  </Link>
            };
            this.state.events.push(event);
        });
    };

    render() {
        const {user} = this.props;
        if (!user) return <Redirect to='/login'/>;

        return (
            <div className="App container">
                <BigCalendar
                    localizer={localizer}
                    defaultDate={new Date()}
                    defaultView="month"
                    events={this.state.events}
                    style={{height: "100vh"}}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        meetupList: state.meetup.meetups,
        user: state.auth.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMeetupList: (meetups) => dispatch(getMeetupList(meetups))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
