import React from 'react';

const LogOut = () => {

    return (
        <div className="container">
            <h4 className="center">Log Out</h4>
        </div>
    );

}

export default LogOut;