import React, {Component} from 'react';
import { connect } from 'react-redux';
import {logInGoogle} from "../../store/actions/authActions";
import config from "../../config";
import {GoogleLogin} from "react-google-login";

class SocialsLogIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: false,
            user: null
        };
    }

    googleResponse = (response) => {
        let data={
            name: response.profileObj.name,
            email:response.profileObj.email
        }
        this.props.logInGoogle(data);
    };

    onFailure = (error) => {
        console.log(error);
    };

    logout = () => {
        this.setState({isAuthenticated: false, token: '', user: null})
    };

    render() {
        let content = !!this.state.isAuthenticated ?
            (
                <div>
                    <p>Authenticated</p>
                    <div>
                        {this.state.user.email}
                    </div>
                    <div>
                        <button onClick={this.logout} className="button">
                            Log out
                        </button>
                    </div>
                </div>
            ) :
            (
                <div className="buttons-container">
                    <GoogleLogin
                        clientId={config.GOOGLE_CLIENT_ID}
                        buttonText="LOGIN WITH GOOGLE"
                        onSuccess={this.googleResponse}
                        onFailure={this.onFailure}
                    />
                </div>
            );
        return (
            <div className="App">
                {content}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        authError: state.auth.authError,
        cookies: ownProps.cookies,
        token: state.auth.token,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logInGoogle: (data) => dispatch(logInGoogle(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SocialsLogIn);