import React, {Component} from 'react';
import {Redirect} from "react-router-dom";
import { connect } from 'react-redux';
import {signUp} from "../../store/actions/authActions";

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    state = {
        email: '',
        name: '',
        password: ''
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.signUp(this.state);
        this.props.history.push('/login');
    };

    render() {
        const { user, authError, authMessage } = this.props;
        if(user) return <Redirect to='/' />;
        return (
            <div className="container sdvig">
                <form onSubmit={this.handleSubmit} className="white">
                    <h5 className="black-text flow-text center-align">SIGN UP</h5>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input type="email" id="email" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="displayName">displayName</label>
                        <input type="text" id="displayName" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="password">Password</label>
                        <input type="password" id="password" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <button className="btn lighten-1 z-depth-0">SIGN UP</button>
                        <div className="red-text center">
                            { authError ? <p>{authError}</p> : null }
                        </div>
                        <div className="green-text center">
                            { authMessage ? <p>{authMessage}</p> : null }
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        authError: state.auth.authError,
        authMessage: state.auth.authMessage,
        user: state.auth.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        signUp: (newUser) => dispatch(signUp(newUser))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
