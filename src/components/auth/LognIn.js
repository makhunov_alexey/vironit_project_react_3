import React, {Component} from 'react';
import { connect } from 'react-redux';
import { logIn } from "../../store/actions/authActions";
import { Redirect} from "react-router-dom";
import {logInGoogle} from "../../store/actions/authActions";
import SocialsLogIn from "./SocialsLogIn";

class LognIn extends Component {
    state = {
        email: '',
        password: ''
    };

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.logIn(this.state);
    };

    render() {
        const { authError,user } = this.props;
        if(user) return <Redirect to='/' />;
        return (
            <div className="container sdvig">
                <form onSubmit={this.handleSubmit} className="white">
                    <h5 className="black-text flow-text center-align">LOG IN</h5>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input type="email" id="email" onChange={this.handleChange}/>
                    </div>

                    <div className="input-field">
                        <label htmlFor="password">Password</label>
                        <input type="password" id="password" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <button className="col offset-s2 btn lighten-1 z-depth-0 ">LOG IN</button>
                        <SocialsLogIn />
                        <div className="red-text center">
                            { authError ? <p>{authError}</p> : null }
                        </div>
                    </div>
                </form>

            </div>
            
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        authError: state.auth.authError,
        cookies: ownProps.cookies,
        authMessage: state.auth.authMessage,
        user: state.auth.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logIn: (creds) => dispatch(logIn(creds)),
        logInGoogle: () => dispatch(logInGoogle())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LognIn);