import React from 'react';
import {Link} from 'react-router-dom';
import moment from "moment";
import MeetupLikes from "../like/MeetupLikes";

const MeetupSummary = ({meetup}) => {
    let meetupDate = moment(Date.parse(meetup.date)).format('h:mm:ss a,MMMM Do YYYY');;

    return (
        <div className="col s10 l4">
            <div className="card z-depth-1 meetup-summary hoverable">
                <div className="card-image ">
                    <MeetupLikes key={"MeetupLikes"+meetup._id} meetup_id = {meetup._id}/>
                </div>
                <div className="card-content">
                <Link to={'/meetup/' + meetup._id} key={meetup._id} >                
                    <p>Posted by <span className="meetup-creator">{meetup.asignee}</span></p>
                    <p>About: {meetup.info}</p>
                    <p>Duration : <i className="grey-text">{meetup.duration}</i>
                    </p>
                    <p>Start at : <i className="grey-text">{meetupDate}</i></p>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default MeetupSummary;