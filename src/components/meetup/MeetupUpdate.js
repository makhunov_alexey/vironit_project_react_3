import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Modal} from 'react-materialize';
import {updateMeetup,getMeetupList} from "../../store/actions/meetupActions";


class MeetupUpdate extends Component {
    constructor(props) {
        super(props);
        this.submitDescriptionUpdate = this.submitDescriptionUpdate.bind(this);
    }

    submitDescriptionUpdate = (e) => {
        const {meetup}=this.props;
        e.preventDefault();
        let formData = {
            action: this.form.url.value,
            newData: {
                updateInfo: this.form.info.value,
                updateDuration: this.form.duration.value,
                updateDate: this.form.date.value,
                meetupId:meetup._id
            }
        };
        this.setState({
            updateInfo: this.form.info.value,
            updateDuration: this.form.duration.value,
            updateDate: this.form.date.value,
        });
        this.props.updateMeetup(formData).then(()=>{
            this.props.getMeetupList();       
        });
    };

    render() {
        const {meetup, meetupInfo} = this.props;
        let updateDescriptionURL;
        if (meetup) {
            updateDescriptionURL = `meetup/${meetup._id}`;
        }
        return (
                <Modal
                    header='Update Description'
                    trigger={<li><i className="medium material-icons updateIcon">edit</i></li>}
                    actions={
                            <Button modal="close" className="col l3 right" default>close</Button>
                    }
                    modalOptions={{open: true}}>

                    <form onSubmit={this.submitDescriptionUpdate} className="white"
                          ref={el => (this.form = el)}>
                        <input type="hidden" id="url" defaultValue={updateDescriptionURL}/>
                        <div className="input-field valign-wrapper">
                            <input type="text" id="info" defaultValue={meetupInfo}/>
                        </div>
                        <div className="input-field">
                            <input type="datetime-local" id="date" className="materialize-textarea"
                                   required/>
                        </div>
                        <div className="input-field">
                            duration
                            <input type="time" id="duration" className="materialize-textarea"/>
                        </div>
                        <div className="input-field">
                            <button className="btn lighten-1 z-depth-0">Update</button> 
                        </div>
                    </form>
                </Modal>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let meetup= ownProps.meetup;
    let meetupInfo= ownProps.meetupInfo;
    let meetupDescription;
    if (state.meetup.meetupDescription) {
        meetupDescription = state.meetup.meetupDescription
    } else {
        meetupDescription = meetup ? meetup.description : state.meetup.description;
    }

    return {
        meetup: meetup,
        meetupDescription: meetupDescription,
        meetupInfo: meetupInfo
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateMeetup: (data) => dispatch(updateMeetup(data)),
        getMeetupList: (meetups) => dispatch(getMeetupList(meetups)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetupUpdate);
