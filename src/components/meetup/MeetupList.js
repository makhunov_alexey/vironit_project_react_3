import React from 'react';
import Meetupinformation from './Meetupinformation';

const MeetupList = ({meetups}) => {
    return (
        <div className="meetup-list">
            {meetups && meetups.map(meetup => {
                return (
                    <Meetupinformation key={"MeetupSummary"+meetup._id} meetup={meetup}/>
                )
            })}
        </div>
    );
};

export default MeetupList;