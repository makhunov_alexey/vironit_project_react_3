import React, {Component} from 'react';
import { connect } from 'react-redux';
import { createMeetup } from "../../store/actions/meetupActions";
import { Redirect} from "react-router-dom";

class CreateMeetup extends Component {

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
        })
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createMeetup(this.state).then(()=>{
            this.props.history.push('/');        
        });
    };

    render() {
        const {user} = this.props;
        if(!user) return <Redirect to='/login' />;
        return (
            <div className="container sdvig">
                <form onSubmit={this.handleSubmit} className="white" >
                    <h5 className="grey-text text-darken-3">Create Meetup</h5>
                    <div className="input-field valign-wrapper">
                        <input type="text" name="info" id="info" onChange={this.handleChange}/>
                        <label htmlFor="info">Title</label>
                    </div>
                    <div className="input-field">
                        <input type="datetime-local" name="date" id="date" className="materialize-textarea" required onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <input type="time" name="duration" id="duration" className="materialize-textarea" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <button className="btn lighten-1 z-depth-0">Create</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.auth.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        createMeetup: (creds) => dispatch(createMeetup(creds))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateMeetup);
