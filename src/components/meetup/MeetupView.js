import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from "react-router-dom";
import moment from "moment";
import {Button, Modal} from 'react-materialize';
import {getComment} from "../../store/actions/commentActions";
import {deleteMeetup} from "../../store/actions/meetupActions";
import {getAttachments} from "../../store/actions/attachmentActions";
import AddFiles from "../attachment/AddFiles";
import MeetupImages from "../attachment/MeetupImages";
import MeetupVideos from "../attachment/MeetupVideos";
import MeetupDocuments from "../attachment/MeetupDocuments";
import MeetupUpdate from "./MeetupUpdate";
import MeetupComments from "../comment/MeetupComments";
import {getUserName} from "../../store/actions/authActions";


class MeetupDetails extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.submitUpdate = this.submitUpdate.bind(this);
        this.deleteMeetup = this.deleteMeetup.bind(this);

        this.state = {
            title: props.title,
            status: props.status,
            userName:''
        };
    }

    componentDidMount()  {
        this.getPropsMeetupView();
        this.getStateMeetupViews();
    }

    getPropsMeetupView = ()=>{
        const {meetup_id} = this.props;
        this.props.getComment(meetup_id);
        this.props.getAttachments(meetup_id);
    }

    getStateMeetupViews = async()=>{
        const {meetup} = this.props;
        if(meetup){
            this.setState({userName:await getUserName(meetup.asignee)})
        }
    }

    handleChange = (e) => {
        e.preventDefault();
    };

    submitUpdate = (e) => {
        e.preventDefault();
    };
    deleteMeetup = (e) => {
        const {meetup_id} = this.props;
        let dataToDelete = {
            meetup_id: meetup_id
        };

        this.props.deleteMeetup(dataToDelete).then(()=>{
            this.props.history.push('/');        
        })
    };

    render() {
        const {meetup, user, meetupInfo,attachments} = this.props;
        if(!user) return <Redirect to='/login' />;
        let userName = this.state.userName;
        let allvideos=[];
        let alldocuments=[];
        let allimages=[];
        attachments.map(el=>{
            el.videos.forEach(element => {
            allvideos.push(element)
            });
            el.documents.forEach(element => {
                alldocuments.push(element)
            });
            el.images.forEach(element => {
                allimages.push(element)
            });
        })
        let meetupDate = moment(Date.parse(meetup.date)).format('h:mm:ss a,MMMM Do YYYY');;
        return (
            <div className="container section meetup-details">
                    <ul className="row">
                        <div className="black-text flow-text center-align">
                            <i>{meetupDate}</i>
                        </div > 
                            <span className="black-text flow-text">{meetupInfo}</span>
                                <div className="black-text flow-text center-align">Posted by {userName}</div>
                                <div className="black-text flow-text center-align">Duration: {meetup.duration}</div>
                    <Modal
                        header='Confirm deletion'
                        trigger={
                            <li className="rowicon">
                                <i className="medium material-icons deleteIcon">cancel</i>
                            </li>
                            }
                        actions={
                            <div>
                                <Button modal="close" onClick={this.deleteMeetup} name={meetup.info}
                                        id={meetup._id}>
                                   delete</Button>
                                <Button modal="close" default>close</Button>
                            </div>
                        }>
                        <p>ARE YOU WANT TO DELETE MEETUP?</p>
                    </Modal>
                    <MeetupUpdate meetup={meetup} meetupTitle={meetup.info}/>
                    <Modal
                        header='Add files'
                        trigger={
                            <li className="rowicon">
                                <i className="medium material-icons addFileIcon">file_upload</i>
                            </li>
                            }
                        actions={
                            <div className="row">
                                <Button modal="close" default>close</Button>
                            </div>
                        }>
                        <AddFiles meetup_id={meetup._id}/>
                    </Modal>
                        { alldocuments.length>0&&
                            <MeetupDocuments meetup_id={meetup._id} alldocuments={alldocuments}/>
                        }
                        { allimages.length>0&&
                            <MeetupImages meetup_id={meetup._id} allimages={allimages}/>
                        }   
                        {allvideos.length>0&&
                            <MeetupVideos meetup_id={meetup._id} allvideos={allvideos}/>
                        }
                        <MeetupComments meetup_id={meetup._id} meetupAsignee={meetup.asignee}/>
                    </ul>
                </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const meetup_id = ownProps.match.params.meetup_id;
    const meetups = state.meetup.meetups;
    const meetup = meetups ? meetups.find(searchingMeetup => searchingMeetup._id === meetup_id) : null;
    let meetupInfo;
    if (state.meetup.meetupInfo) {
        meetupInfo = state.meetup.meetupInfo
    } else {
        meetupInfo = meetup ? meetup.info : state.meetup.info;
    }

    let updateDuration = state.meetup.meetupDuration ? state.meetup.meetupDuration : (meetup ? meetup.duration : state.meetup.duration);
    let updateDate = state.meetup.updateDate ? state.meetup.updateDate : (meetup ? meetup.date : state.meetup.date);

    const comments = state.comment.comments;
    return {
        meetup_id: meetup_id,
        meetup: meetup,
        meetupInfo: meetupInfo,
        updateDate: updateDate,
        updateDuration: updateDuration,
        comments: comments,
        user: state.auth.user,
        meetupMessage: state.meetup.meetupMessage,
        attachments: state.attachment.attachments
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getComment: (meetup_id) => dispatch(getComment(meetup_id)),
        deleteMeetup: (name) => dispatch(deleteMeetup(name)),
        getAttachments: (meetup_id) => dispatch(getAttachments(meetup_id)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetupDetails);
