import React, {Component} from 'react';
import MeetupList from './MeetupList';
import {connect} from 'react-redux';
import {getMeetupList} from '../../store/actions/meetupActions';
import {Redirect} from 'react-router-dom';
import {getUserLikes} from "../../store/actions/authActions";

class AllMeetups extends Component {

    componentDidMount() {
        this.getPropsAllMeetups();
    }

    getPropsAllMeetups = ()=>{
        this.props.getMeetupList();
        this.props.getUserLikes();
    }
    
    render () {
        const {meetups, user} = this.props;
        if(!user) return <Redirect to='/login' />;
        return (
            <div className="dashboard container">
                <div className="row">
                    <div>
                        <MeetupList meetups={meetups}/>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        meetups: state.meetup.meetups,
        user: state.auth.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMeetupList: () => dispatch(getMeetupList()),
        getUserLikes: () => dispatch(getUserLikes())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AllMeetups);