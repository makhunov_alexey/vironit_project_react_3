import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getMeetupList} from "../../store/actions/meetupActions";
import {likeMeetup} from "../../store/actions/authActions";

class MeetupLikes extends Component {

    getPropsMeetupLikes = ()=>{
        let {meetup_id} = this.props;
        this.props.likeMeetup(meetup_id);
        this.props.getMeetupList();
    }

    render() {
        let {meetup_id, userLikedMeetups} = this.props;
        const meetupAmongUserLiked = userLikedMeetups?userLikedMeetups.indexOf(meetup_id): -1;
        let showLike = meetupAmongUserLiked !== -1;
        let likeIcon;
        if (showLike) {
            likeIcon = <button className="btn-floating halfway-fab red " onClick={this.getPropsMeetupLikes}>
                <i className="material-icons like">favorite</i>
            </button>
        } else {
            likeIcon = <button className="btn-floating halfway-fab grey" onClick={this.getPropsMeetupLikes}>
                <i className="material-icons like-disabled dislike">favorite_border</i>
            </button>
        }
        return (
            <div>
                {likeIcon}
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let user = state.auth.user;
    let meetup_id = ownProps.meetup_id;
    let userLikedMeetups = state.auth.userLikedMeetups;
    return {
        user: user,
        meetup_id: meetup_id,
        userLikedMeetups: userLikedMeetups,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        likeMeetup: (meetup_id) => dispatch(likeMeetup(meetup_id)),
        getMeetupList: (meetups) => dispatch(getMeetupList(meetups))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetupLikes);
