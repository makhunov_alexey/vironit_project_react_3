import React from 'react';
import { NavLink, withRouter} from 'react-router-dom';

const SignedOutLinks = () => {
    return (
        <ul className="right hide-on-med-and-down">
            <li><NavLink to="/login">LOG IN</NavLink></li>
            <li><NavLink to="/signup">SIGN UP</NavLink></li>
        </ul>
    );
};

export default withRouter(SignedOutLinks);