import React from 'react';
import { NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import { logOut} from "../../store/actions/authActions";
import { Button, Modal} from 'react-materialize'


const SignedInSidebarLinks = (props) => {

    document.addEventListener('DOMContentLoaded', function() {});
    return (
        <div className='row'>
            <ul className="right hide-on-med-and-down">
                <li>
                    <NavLink to="/" className="nav-links ">
                    {props.userName}
                    </NavLink>
                </li>
                <li><NavLink to="/createmeetup" className="nav-links">New Meetup</NavLink></li>
                <li><NavLink to="/calendar" className="nav-links">Calendar</NavLink></li>
                <li>
                    <Modal
                        header='Logging out'
                        trigger={<a onClick={props.logOut}>LOG OUT</a>}
                        actions={
                            <div>
                                <Button modal="close" onClick={props.logOut}>LOGOUT</Button>
                                <Button modal="close" default>close</Button>
                            </div>
                        }>
                        <p>ARE YOU WANT TO LOG OUT?</p>
                    </Modal>
                </li>
            </ul>
        </div>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => dispatch(logOut())
    }
};

export default connect(null, mapDispatchToProps)(SignedInSidebarLinks);