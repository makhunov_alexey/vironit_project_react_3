import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';
import {connect} from 'react-redux';
import { SideNav} from 'react-materialize'

const Navbar = (props) => {
    const {user} = props;

    const links = user ? <SignedInLinks userName={user.displayName} userEmail={user.email}/> : <SignedOutLinks/>;
    return (
        <nav className="nav-wrapper z-depth-2 fixed">
            <div className="container">
                <Link to="/" className="brand-logo hide-on-med-and-down nav-links">MEETUP</Link>
                <SideNav
                    trigger={<Link to="#">
                        <i className="material-icons hide-on-large-only black-text">menu</i>
                    </Link>}
                    className="hide-on-large-only"
                    options={{closeOnClick: true}}
                >
                </SideNav>
                {links}
            </div>
        </nav>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.auth.user
    }
};

export default connect(mapStateToProps, null)(withRouter(Navbar));