import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from "moment";
import {Comment, Tooltip} from 'antd';
import {updateComment, getComment, deleteComment} from "../../store/actions/commentActions";
import {getUserName} from "../../store/actions/authActions";

class MeetupComment extends Component {
    constructor(props) { 
        super(props);       
        this.state = {
            userName: ''
        };
    }
    componentDidMount() {
        this.setStateComments();
        this.deleteCommentButtons();
        this.updateCommentButtons();
    }

    setStateComments = async()=>{
        const {comment} = this.props;
        this.setState({userName: await getUserName(comment._user)});
    }

    deleteCommentButtons = ()=>{
        const {user} = this.props;
        let deleteCommentButtons = document.querySelectorAll('.delete-comment');
        deleteCommentButtons.forEach((button) => {
            let commentAuthor = button.getAttribute('author');
            if (user && commentAuthor !== user._id) {
                button.classList.add('hide')
            }
        });
    }

    updateCommentButtons = ()=>{
        const {user} = this.props;
        let updateCommentButtons = document.querySelectorAll('.update-comment');
        updateCommentButtons.forEach((button) => {
            let commentAuthor = button.getAttribute('author');
            if (user && commentAuthor !== user._id) {
                button.classList.add('hide')
            }
        })
    }

    setRepplyCredentials = (e) => {
        let button = e.target;
        let relatedCommentId = document.getElementById('related_comment_id');
        let relatedCommentAuthor = document.getElementById('related_comment_author');
        let commentField = document.getElementById('comment');

        relatedCommentId.value = button.getAttribute('id');
        relatedCommentAuthor.value = button.getAttribute('name');
        commentField.value = button.getAttribute('name') + ", ";
    };

    updateComment = (e) => {
        let button = e.target;
        const {comment} = this.props;
        let relatedCommentId = document.getElementById('related_comment_id');
        let updatingCommentId = document.getElementById('updating_comment_id');
        let commentField = document.getElementById('comment');

        relatedCommentId.value = button.getAttribute('name');
        updatingCommentId.value = button.getAttribute('name');
        commentField.value = comment.commentBody;
    };

    render() {
        const {comment,allcomments} = this.props;
        const userName = this.state.userName
        if(comment){
            let commentCreationDate;
            if(comment.createdAt){
                commentCreationDate = moment(Date.parse(comment.createdAt)).format('MMMM Do YYYY, h:mm:ss a');
            }else {commentCreationDate="now"}
            return (
                <Comment
                    actions={[
                        <i className="btn-flat-small disabled" id={comment._id}
                           name={userName} onClick={this.setRepplyCredentials}>REPLY</i>,
    
                        <i className=" btn-flat-small disabled update-comment"
                           author={comment._user}
                           onClick={this.updateComment} name={comment._id}>UPDATE</i>,
                        <i className="waves-effect waves-red btn-flat-small red-text disabled delete-comment"
                           author={comment._user} name={comment._id}>DELETE</i>]}
                    author={<button className="btn-flat disabled">{userName}</button>}
                    content={<p>{comment.commentBody}</p>}
                    datetime={(
                        <Tooltip>
                            <i> {commentCreationDate}</i>
                        </Tooltip>
                    )}>
                    {comment.relatedComments && comment.relatedComments.map((comment) => {
                        const commentinformation = allcomments.find(element => element._id == comment);
                        return (
                            <MeetupComment key={"comment" + comment} comment={commentinformation} allcomments={allcomments}/>
                        )
                    })}
                </Comment>
            )    
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    let user = state.auth.user;
    let comment = ownProps.comment;
    let meetup_id = ownProps.meetup_id;
    return {
        state: state,
        comment: comment,
        user: user,
        meetup_id: meetup_id,
        allcomments:ownProps.allcomments,
        meetupAsignee:ownProps.meetupAsignee
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteComment: (comment_id) => dispatch(deleteComment(comment_id)),
        getComment: (meetup_id) => dispatch(getComment(meetup_id)),
        updateComment: (meetup_id) => dispatch(updateComment(meetup_id)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetupComment);
