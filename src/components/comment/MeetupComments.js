import React, {Component} from 'react';
import {connect} from 'react-redux';
import MeetupComment from "./Comments";
import {Button, Icon, Modal} from 'react-materialize';
import {getComment, createComment, updateComment, deleteComment} from "../../store/actions/commentActions";
import { withRouter } from 'react-router-dom';

class MeetupComments extends Component {

    componentDidMount() {
        this.getPropsMeetupComments();
        this.scrollToBottom();
    }

    getPropsMeetupComments = ()=>{
        const {meetup_id} = this.props;
        if (meetup_id) {
            this.props.getComment(meetup_id);
        }
    }

    scrollToBottom() {
        this.el.scrollIntoView({ behavior: 'smooth' });
    }

    handleSubmit = (e) => {
        const {meetup_id} = this.props;
        e.preventDefault();
        if((this.form.updating_comment_id.value)) {
            let commentData = {
                comment_id: this.form.updating_comment_id.value,
                commentBody: this.form.comment.value,
                meetup_id: meetup_id
            };
            this.props.updateComment(commentData);
            this.form.comment.value = '';
        } else {
            let commentData = {
                meetup_id: meetup_id,
                relatedCommentId: this.form.related_comment_id.value,
                comment: this.form.comment.value
            };
            this.props.createComment(commentData).then(()=>{
                this.setState(this.state);
            });
            this.form.comment.value = '';
        }
    };

    handleClick = (e) => {
        let element = e.target;
        let modalTriggerButton= document.querySelector('#delete_comment_confirmation_trigger');
        let i = element.closest("i");
        if (!i) return;
        if(i.classList.contains('red-text')) {
            let comment_id = i.getAttribute('name');
            let deleteCommentConfirmationButton= document.querySelector('#delete_comment_confirmation_button');
            deleteCommentConfirmationButton.setAttribute('name', comment_id);
            modalTriggerButton.click();
        }
    };

    deleteComment = (e) => {
        const {meetup_id} = this.props;
        let button = e.target;
        let comment_id = button.getAttribute('name');
        const data={
            meetup_id: meetup_id,
            comment_id:comment_id
        }
        this.props.deleteComment(data);
    };

    render() {
        const {comments,allcomments,meetupAsignee} = this.props;
        let maincomments;
        if (comments){
            maincomments = comments.filter(element=>element._relatedCommentId=="000000000000000000000000")
        }
        return (
            <div>
                <div className="card document-block comments">
                    <ul className="collection with-header">
                    <li className="collection-header"><h4>Comments</h4></li>
                    <div className="card-content comment-block" id="comment_block" onClick={this.handleClick}>
                        {maincomments && maincomments.map(comment => {
                            return (
                                <MeetupComment key={"MeetupComment"+comment._id} comment={comment} allcomments={allcomments} meetupAsignee={meetupAsignee}/>
                            )
                        })}
                        <div ref={el => { this.el = el; }}></div>
                    </div>
                    </ul>
                </div>
                <div className="card document-block comments">
                    <form onSubmit={this.handleSubmit} className="white container" ref={el => (this.form = el)}>
                        <div>
                            <input type="hidden" id="updating_comment_id" name="updating_comment_id" defaultValue=''/>
                            <input type="hidden" id="related_comment_id" name="related_comment_id" defaultValue='000000000000000000000000' />
                            <input type="hidden" id="related_comment_author" name="related_comment_author" defaultValue='' />
                                COMMENT:
                            <input type="text" id="comment" name="comment" className="autocomplete" placeholder='Add comment...'/>
                            <button className="btn blue  z-depth-0 commentbutton" name={'comment._id'} >
                                SEND
                            </button>
                        </div>
                    </form>
                </div>
                <Modal
                    header='Confirm deletion' ref={el => (this.modal = el)}
                    trigger={<button className="waves-effect waves-red btn-flat-small red-text hide" id='delete_comment_confirmation_trigger'
                                onClick={this.deleteComment} name={'comment._id'}>Delete</button>}
                    id='delete_comment_confirmation'
                    actions={
                        <div>
                            <Button modal="close" onClick={this.deleteComment} id='delete_comment_confirmation_button'
                                    name='' waves="light" className="red darken-2"><Icon
                                left>delete</Icon>delete</Button>
                            <Button modal="close" default>close</Button>
                        </div>
                    }>
                    <p>ARE YOU WANT TO DELETE COMMENT?</p>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        meetup_id: ownProps.meetup_id,
        comments: state.comment.comments,
        allcomments:state.comment.allcomments,
        meetupAsignee:ownProps.meetupAsignee
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getComment: (meetup_id) => dispatch(getComment(meetup_id)),
        createComment: (commentData) => dispatch(createComment(commentData)),
        updateComment: (meetup_id) => dispatch(updateComment(meetup_id)),
        deleteComment: (data) => dispatch(deleteComment(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MeetupComments));
