import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Icon, Modal} from 'react-materialize';
import {deleteDocument} from "../../store/actions/attachmentActions";
import {getAttachments} from "../../store/actions/attachmentActions";
import {getUser} from "../../store/actions/authActions";

class MeetupDocuments extends Component {
    constructor(props) {
        super(props);
        this.deleteDocument = this.deleteDocument.bind(this);
    }

    componentDidMount()  {
        this.getPropsMeetupDocuments();
    }

    getPropsMeetupDocuments = ()=>{
        const {user} = this.props;
        if(user){
            this.props.getUser(user._id);
        }
    }

    deleteDocument = async (e) => {
        let button = e.target;
        const {meetup_id} = this.props;
        let documentLink = `${button.getAttribute('name')}`;
        let documentFullName = documentLink.substr(documentLink.lastIndexOf('/') + 1);

        let dataToDelete = {
            attachment_id: button.getAttribute('id'),
            file_name: documentFullName
        };
        await this.props.deleteDocument(dataToDelete);
        await this.props.getAttachments(meetup_id);
    };

    render() {
        const { user, alldocuments, attachments} = this.props;
        return (
            <div className="card document-block">
                <ul className="collection with-header">
                    <li className="collection-header"><h4>Documents</h4></li>
                    {alldocuments && alldocuments.map((document) => {
                    let documentLink = `${process.env.REACT_APP_API_URL}/${document}`;
                    let attachmentDocument = attachments.find(element=>element.documents.find(el=>el==document))
                    const isUsersAttachment = user.attachments.find(element=>element==attachmentDocument._id);
                    let access = user.role === 'Admin' || isUsersAttachment!==undefined;

                        return (
                            <li key={"meetupDocument"+documentLink} className="collection-item">
                                <div>{document}  </div>
                                <div className="document-buttons">
                                    <a href={documentLink} className="secondary-content">
                                        <i className="material-icons">arrow_forward</i>
                                    </a>
                                    {access === true &&
                                    <Modal
                                        header='Confirm deletion'
                                        trigger={<a href={documentLink} className="secondary-content">
                                            <i className="material-icons document-delete-icon">delete</i>
                                        </a>}
                                        actions={
                                            <div>
                                                <Button modal="close" onClick={this.deleteDocument} name={documentLink}
                                                        id={attachmentDocument._id} waves="light" className="red darken-2" ><Icon left>delete</Icon>delete</Button>
                                                <Button modal="close" default>close</Button>
                                            </div>
                                        }>
                                        <p>Are you sure you want to delete this file?</p>
                                    </Modal>
                                    }
                                </div>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.auth.user,
        meetup_id: ownProps.meetup_id,
        attachments: state.attachment.attachments,
        alldocuments:ownProps.alldocuments
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteDocument: (data) => dispatch(deleteDocument(data)),
        getAttachments: (meetup_id) => dispatch(getAttachments(meetup_id)),
        getUser: (userid) => dispatch(getUser(userid))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetupDocuments);
