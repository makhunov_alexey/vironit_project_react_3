import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Icon, Modal} from 'react-materialize';
import {deleteVideo} from "../../store/actions/attachmentActions";
import {getAttachments} from "../../store/actions/attachmentActions";
import {getUser} from "../../store/actions/authActions";

class MeetupVideos extends Component {
    constructor(props) {
        super(props);
        this.deleteVideo = this.deleteVideo.bind(this);
    }
    componentDidMount()  {
        this.getPropsMeetupVideos();
    }

    getPropsMeetupVideos= ()=>{
        const {user} = this.props;
        if(user){
            this.props.getUser(user._id);
        }
    }

    deleteVideo = async (e) => {
        let button = e.target;
        const {meetup_id} = this.props;
        let documentLink = `${button.getAttribute('name')}`;
        let documentFullName = documentLink.substr(documentLink.lastIndexOf('/') + 1);
        let dataToDelete = {
            attachment_id: button.getAttribute('id'),
            file_name: documentFullName
        };
        await this.props.deleteVideo(dataToDelete);
        await this.props.getAttachments(meetup_id);
    };

    render() {
        const { user, attachments,allvideos} = this.props;
        return (
            <div className="card document-block">
                <ul className="collection with-header">
                <li className="collection-header"><h4>Videos</h4></li>

                {allvideos && allvideos.map((video) => {
                    let videoLink = `${process.env.REACT_APP_API_URL}/${video}`;
                    let attachmentVideo = attachments.find(element=>element.videos.find(el=>el==video))
                    const isUsersAttachment = user.attachments.find(element=>element==attachmentVideo._id);
                    let access = user.role === 'Admin' || isUsersAttachment!==undefined;
                    return (
                        <div key={"meetupVideo" + videoLink} className="col s12 m6">
                            <div className="card">
                                <div className="card-image">
                                    <video width="100%" height="auto" className="responsive-video"
                                           key={videoLink} controls>
                                        <source src={videoLink} />
                                    </video>
                                    {access === true &&
                                    <Modal
                                        header='Confirm deletion'
                                        trigger={<button className="btn-floating halfway-fab waves-effect waves-light white"
                                                    onClick={this.deleteVideo}>
                                            <i className="material-icons icon-red">clear</i></button>}

                                        actions={
                                            <div>
                                                <Button modal="close" onClick={this.deleteVideo} name={videoLink}
                                                        id={attachmentVideo._id} waves="light" className="red darken-2" ><Icon left>delete</Icon>delete</Button>
                                                <Button modal="close" default>close</Button>
                                            </div>
                                        }>
                                        <p>Are you sure you want to delete this file?</p>
                                    </Modal>
                                    }
                                </div>
                            </div>
                        </div>
                    )
                })}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.auth.user,
        meetup_id: ownProps.meetup_id,
        attachments: state.attachment.attachments,
        allvideos:ownProps.allvideos
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteVideo: (data) => dispatch(deleteVideo(data)),
        getAttachments: (meetup_id) => dispatch(getAttachments(meetup_id)),
        getUser: (userid) => dispatch(getUser(userid))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetupVideos);
