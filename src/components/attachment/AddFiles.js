import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createMeetup } from "../../store/actions/meetupActions";
import {Redirect} from "react-router-dom";
import {addFiles} from "../../store/actions/attachmentActions";
import {getAttachments} from "../../store/actions/attachmentActions";

class AddFiles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            startDate: '',
            endDate: '',
            upload: ''
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value,
        })
    };
    
    handleSubmit = async (e) => {
        e.preventDefault();
        const {user_id, meetup_id} = this.props;
        let formData = {
            action: this.form.url.value,
            user_id:user_id,
            meetup_id:meetup_id,
            newData: {
                upload: this.form.upload.files
            }
        };
        await this.props.addFiles(formData).then(()=>{
            this.props.getAttachments(meetup_id);
        })
    };

    render() {
        const {meetup_id,user} = this.props;
        const addFilesURL = `${process.env.REACT_APP_API_URL}/attachment/${meetup_id}`;
        if(!user) return <Redirect to='/login' />;
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit} className="white add-files" ref={el => (this.form = el)}>
                    <input type="hidden" id="url" defaultValue={addFilesURL} />
                    <div className="file-field input-field">
                        <div className="btn">
                            <span>File</span>
                            <input type="file" ref={this.input} id="upload" className="materialize-textarea" onChange={this.handleChange} required multiple/>
                        </div>
                        <div className="file-path-wrapper">
                            <input className="file-path validate" type="text" placeholder="Upload one or more files"/>
                        </div>
                    </div>
                    <div className="input-field">
                        <button className="btn red lighten-1 z-depth-0">Add</button>
                    </div>
                </form>

            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.auth.user,
        meetup_id: ownProps.meetup_id,
        user_id: state.auth.user._id
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        createMeetup: (meetup) => dispatch(createMeetup(meetup)),
        addFiles: (formData) => dispatch(addFiles(formData)),
        getAttachments: (meetup_id) => dispatch(getAttachments(meetup_id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddFiles);
