import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Icon, Modal, MediaBox} from 'react-materialize';
import {deleteImage} from "../../store/actions/attachmentActions";
import {getAttachments} from "../../store/actions/attachmentActions";
import {getUser} from "../../store/actions/authActions";

class MeetupImages extends Component {
    constructor(props) {
        super(props);
        this.deleteImage = this.deleteImage.bind(this);
        this.state = {
            active: 0
        };
    }
    componentDidMount() {
        this.getPropsMeetupImages();
    }

    getPropsMeetupImages = ()=>{
        const {user} = this.props;
        if(user){
            this.props.getUser(user._id);
        }
    }

    deleteImage = async (e) => {
        let button = e.target;
        let {meetup_id} = this.props;
        let documentLink = `${button.getAttribute('name')}`;
        let documentFullName = documentLink.substr(documentLink.lastIndexOf('/') + 1);
        let dataToDelete = {
            attachment_id: button.getAttribute('id'),
            file_type: 'images',
            file_name: documentFullName
        };
        await this.props.deleteImage(dataToDelete);
        await this.props.getAttachments(meetup_id);
    };

    render() {
        const { user, attachments, allimages} = this.props;
        return (
            <div className="card document-block">
                <ul className="collection with-header">
                <li className="collection-header"><h4>Images</h4></li>
                {allimages && allimages.map((image) => {
                let imageLink = `${process.env.REACT_APP_API_URL}/${image}`;
                let attachmentImage = attachments.find(element=>element.images.find(el=>el==image))
                const isUsersAttachment = user.attachments.find(element=>element==attachmentImage._id);
                let access = user.role === 'Admin' || isUsersAttachment!==undefined;
                return (
                    <div key={"meetupVideo" + imageLink} className="col s12 m6">
                        <div className="card">
                            <div className="card-image">
                            <MediaBox src={imageLink} key={imageLink} alt={imageLink} caption={image} width="350" className="responsive-img"/>
                                {access === true &&
                                <Modal
                                    header='Confirm deletion'
                                    trigger={<button className="btn-floating halfway-fab waves-effect waves-light white"
                                                onClick={this.deleteImage}>
                                        <i className="material-icons icon-red">clear</i></button>}

                                    actions={
                                        <div>
                                            <Button modal="close" onClick={this.deleteImage} name={imageLink}
                                                    id={attachmentImage._id} waves="light" className="red darken-2"><Icon
                                                left>delete</Icon>delete</Button>
                                            <Button modal="close" default>close</Button>
                                        </div>
                                    }>
                                    <p>Are you sure you want to delete this file?</p>
                                </Modal>
                                }
                            </div>
                        </div>
                    </div>
                )
            })}
            </ul>
        </div>

        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.auth.user,
        meetup_id: ownProps.meetup_id,
        attachments: state.attachment.attachments,
        allimages:ownProps.allimages
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteImage: (data) => dispatch(deleteImage(data)),
        getAttachments: (meetup_id) => dispatch(getAttachments(meetup_id)),
        getUser: (userid) => dispatch(getUser(userid))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetupImages);
