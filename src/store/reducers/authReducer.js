const initState = {
    authMessage: null,
    authError: null,
    user: null
};

const authReducer = (state = initState, action) => {

    switch (action.type) {
        case 'GET_COOKIES_ERROR':
            return {
                ...state
            };
        case 'GET_COOKIES':
            return {
                ...state
            };
        case 'LOGIN_ERROR':
            return {
                ...state,
                authError: 'Login failed'
            };
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                authError: null,
                user: action.logInData.user,
                meetupLikes: action.logInData.user.meetupLikes,
            };
        case 'LOGOUT_ERROR':
            return state;
        case 'LOGOUT_SUCCESS':
            return {
                ...state,
                user: null
            };
        case 'REGISTRATION_SUCCESS':
            return {
                ...state,
                authMessage: action.responseData.message
            };
        case 'REGISTRATION_ERROR':
            return {
                ...state,
                authError: action.message
            };
        case 'GET_USER_SUCCESS':
            return {
                ...state,
                user: action.logInData.user
            };
        case 'GET_USER_ERROR':
            return state;
        case 'GET_USER_BY_ID':
               return {
                ...state,
                user: action.response.data.data,
                meetupLikes: action.response.data.data.meetupLikes
            };
        case 'GET_USER_LIKES':
            return {
                ...state,
                userLikedMeetups: action.userLikedMeetups,
            };
        case 'LIKE_MEETUP':
            return {
                ...state,
                userLikedMeetups: action.userLikedMeetups
            };
        case 'LIKE_MEETUP_ERROR':
            return state;
        default:
            return state;
    }
};

export default authReducer;