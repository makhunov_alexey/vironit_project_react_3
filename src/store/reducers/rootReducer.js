import { combineReducers } from 'redux';
import authReducer from './authReducer';
import meetupReducer from './meetupReducer';
import commentReducer from './commentReducer';
import attachmentReducer from './attachmentReducer';


const rootReducer = combineReducers({
    auth: authReducer,
    meetup: meetupReducer,
    comment: commentReducer,
    attachment: attachmentReducer
});


export default rootReducer;