const initState = {
    meetupMessage: null,
    attachments: []
};

const attachmentReducer = (state = initState, action) => {

    switch (action.type) {
        case 'VIEW_PRELOADER':
            return {
                ...state,
            };
        case 'ADDITION_DOCUMENTS':
            return {
                ...state,
                attachment_id: action.response.data.data._id,
                meetupMessage: action.response.data.message
            };
        case 'ADDITION_IMAGES':
            return {
                ...state,
                attachment_id: action.response.data.data._id,
                meetupMessage: action.response.data.message
            };
        case 'ADDITION_VIDEOS':
            return {
                ...state,
                attachment_id: action.response.data.data._id,
                meetupMessage: action.response.data.message
            };
        case 'ADDITION_FILES_ERROR':
            return state;
        case 'DELETE_IMAGE':
            return state;
        case 'DELETE_IMAGE_ERROR':
            return state;
        case 'DELETE_DOCUMENT':
            return state;
        case 'DELETE_DOCUMENT_ERROR':
            return state;
        case 'DELETE_VIDEO':
            return state;
        case 'DELETE_VIDEO_ERROR':
            return state;
        case 'GET_ATTACHMENTS':
            return {
                ...state,
                attachments: action.response.data.data
            };
        case 'GET_ATTACHMENTS_ERROR':
            return state;
        default:
            return state;
    }
};

export default attachmentReducer;