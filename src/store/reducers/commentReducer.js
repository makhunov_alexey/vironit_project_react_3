const initState = {
    comments: null,
    allcomments: null
};

const commentReducer = (state = initState, action) => {

    switch (action.type) {
        case 'GET_COMMENTS_SUCCESS':
            return {
                ...state,
                comments: action.response.data.data[0],
                allcomments: action.response.data.data[1]
            };
        case 'GET_COMMENTS_ERROR':
            return state;
        case 'CREATE_COMMENT':
            return {
                ...state,
                comments: action.response.data.data[0],
                allcomments: action.response.data.data[1]
            };
        case 'CREATE_COMMENT_ERROR':
            return state;
        case 'UPDATE_COMMENT':
            return {
                ...state,
                comments: action.response.data.data[0],
                allcomments: action.response.data.data[1]
            };
        case 'UPDATE_COMMENT_ERROR':
            return state;
        case 'DELETE_COMMENT':
            return {
                ...state,
                comments: action.response.data.data[0],
                allcomments: action.response.data.data[1]
            };
        case 'DELETE_COMMENT_ERROR':
            return state;
        default:
            return state;
    }
};

export default commentReducer;