const initState = {
    meetups: [],
    authMessage: null,
    meetupMessage: null,
    meetupInfo: null,
    updateDate: null,
    updateDuration: null
};

const meetupReducer = (state = initState, action) => {

    switch (action.type) {
        case 'CREATE_MEETUP':
            return {
            ...state,
            meetups: [...state.meetups, action.response.data.data],
            authMessage: action.response.data.message
            };
        case 'CREATE_MEETUP_ERROR':
            return state;
        case 'GET_MEETUP_LIST':
            return {
                ...state,
                meetups: action.meetupList
            };
        case 'GET_MEETUP_LIST_ERROR':
            return state;
        case 'UPDATE_MEETUP_DESCRIPTION':
            return {
                ...state,
                meetupMessage: action.response.data.message,
                meetupInfo: action.response.data.data.updateInfo,
                updateDate: action.response.data.data.updateDate,
                updateDuration: action.response.data.data.updateDuration
            };
        case 'UPDATE_MEETUP_DESCRIPTION_ERROR':
            return state;
        case 'DELETE_MEETUP_DESCRIPTION':
            return state;
        case 'DELETE_MEETUP_DESCRIPTION_ERROR':
            return state;
        default:
            return state;
    }

};

export default meetupReducer;