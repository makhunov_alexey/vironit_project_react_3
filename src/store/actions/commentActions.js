import axios from "axios";

export const getComment = (meetup_id) => {
    return async (dispatch) => {
        try {
            const response = await axios.get(`${process.env.REACT_APP_API_URL}/comment/${meetup_id}`);
            dispatch({type: 'GET_COMMENTS_SUCCESS', response});
        } catch (error) {
            dispatch({type: 'GET_COMMENTS_ERROR', error});
        }
    }
};

export const createComment = (commentData) => {
    return async (dispatch,getState) => {
        try {
            const getstate = getState();
            let data = {
                commentBody: commentData,
                userId: getstate.auth.user._id
            };
            const result = await axios.post(`${process.env.REACT_APP_API_URL}/commentMeetup`, data);
            if (result){
                const response = await axios.get(`${process.env.REACT_APP_API_URL}/comment/${commentData.meetup_id}`);
                dispatch({type: 'GET_COMMENTS_SUCCESS', response});
            } else {
                dispatch({type: 'GET_COMMENTS_ERROR'});
            }
        } catch (error) {
            dispatch({type: 'CREATE_COMMENT_ERROR', error});
        }
    }

};

export const updateComment = (commentData) => {
    return async (dispatch,getState) => {
        try {
            const getstate= getState();
            let data = {
                commentBody: commentData.commentBody,
                user:getstate.auth.user._id
            };
            const result = await axios.put(`${process.env.REACT_APP_API_URL}/comment/${commentData.comment_id}`, data);
            if(result){
                const response = await axios.get(`${process.env.REACT_APP_API_URL}/comment/${commentData.meetup_id}`);
                dispatch({type: 'UPDATE_COMMENT', response});
            }
        } catch (error) {
            dispatch({type: 'UPDATE_COMMENT_ERROR', error});
        }
    }
};

export const deleteComment = (data) => {
    return async (dispatch,getState) => {
        try {
            const getstate= getState();
            const result = await axios.delete(`${process.env.REACT_APP_API_URL}/deletecomment/${data.comment_id}/${getstate.auth.user._id}`);
            if(result){
                const response = await axios.get(`${process.env.REACT_APP_API_URL}/comment/${data.meetup_id}`);
                dispatch({type: 'DELETE_COMMENT', response});
            }
        } catch (error) {
            dispatch({type: 'DELETE_COMMENT_ERROR', error});
        }
    }
};
