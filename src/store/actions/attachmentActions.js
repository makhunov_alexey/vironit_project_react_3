import axios from "axios";

export const addFiles = (dataToUpdate) => {
    return async (dispatch) => {
        try {
            let addFilesURL = dataToUpdate.action;
            const data = new FormData();
            data.append('meetup_id ', dataToUpdate.action);
            for (let value of Object.values(dataToUpdate.newData.upload)) {
                data.append('upload', value, value.name);
            }
            const response = await axios.post(`${addFilesURL}/${dataToUpdate.user_id}`, data);
            if(response.data.data.documents.length) {
                dispatch({type: 'ADDITION_DOCUMENTS', response});
            }
            if(response.data.data.images.length) {
                dispatch({type: 'ADDITION_IMAGES', response});
            }
            if(response.data.data.videos.length) {
                dispatch({type: 'ADDITION_VIDEOS', response});
            }
        } catch (error) {
            dispatch({type: 'ADDITION_FILES_ERROR', error});
        }
    }
};

export const deleteImage = (dataToDelete) => {
    return async (dispatch) => {
        try {
            if (dataToDelete.attachment_id){
                await axios.delete(`${process.env.REACT_APP_API_URL}/deleteImagesAttachment/${dataToDelete.attachment_id}/${dataToDelete.file_name}`);
                dispatch({type: 'DELETE_IMAGE'});    
            }else {
                dispatch({type: 'DELETE_IMAGE_ERROR'});
            }
        } catch (error) {
            dispatch({type: 'DELETE_IMAGE_ERROR', error});
        }
    }
};

export const deleteDocument = (dataToDelete) => {
    return async (dispatch) => {
        try {
            if (dataToDelete.attachment_id){
                await axios.delete(`${process.env.REACT_APP_API_URL}/deleteDocumentsAttachment/${dataToDelete.attachment_id}/${dataToDelete.file_name}`);
                dispatch({type: 'DELETE_DOCUMENT'});
            }else{
                dispatch({type: 'DELETE_DOCUMENT_ERROR'});
            }
        } catch (error) {
            dispatch({type: 'DELETE_DOCUMENT_ERROR', error});
        }
    }
};

export const deleteVideo = (dataToDelete) => {
    return async (dispatch) => {
        try {
            if (dataToDelete.attachment_id){
                await axios.delete(`${process.env.REACT_APP_API_URL}/deleteVideosAttachment/${dataToDelete.attachment_id}/${dataToDelete.file_name}`);
                dispatch({type: 'DELETE_VIDEO'});
            }else {
                dispatch({type: 'DELETE_VIDEO_ERROR'});
            }
        } catch (error) {
            dispatch({type: 'DELETE_VIDEO_ERROR', error});
        }
    }
};

export const getAttachments = (meetup_id) => {
    return async (dispatch) => {
        try {
            const response = await axios.get(`${process.env.REACT_APP_API_URL}/attachment/${meetup_id}`);
            dispatch({type: 'GET_ATTACHMENTS', response});
        } catch (error) {
            dispatch({ type: 'GET_ATTACHMENTS_ERROR', error });
        }
    }
};

