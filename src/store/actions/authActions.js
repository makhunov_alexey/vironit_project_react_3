import axios from "axios";
import {Cookies} from 'react-cookie';
import {Redirect} from "react-router-dom";
import React from 'react';

const cookies = new Cookies();

export const signUp = (newUser) => {
    return async (dispatch) => {
        try {
            const response = await axios.post(`${process.env.REACT_APP_API_URL}/registration`, newUser);
            const responseData = response.data;
            cookies.set('accessToken', responseData.data);
            if(!response.data.data) {
                let message = response.data.message;
                dispatch({type: 'SIGNUP_ERROR', message});
                return;
            }
            dispatch({type: 'SIGNUP_SUCCESS', responseData});
        } catch (error) {
            console.log(error.message);
            let message = error.message;
            dispatch({type: 'SIGNUP_ERROR', message});
        }
    }
};

export const logIn = (credentials) => {

    return async (dispatch) => {
        try {
            const response = await axios.post(`${process.env.REACT_APP_API_URL}/login`, credentials);
            let logInData = {
                user: response.data.data,
            };
            dispatch({type: 'LOGIN_SUCCESS', logInData});
        } catch (error) {
            console.log(error.message);
            dispatch({type: 'LOGIN_ERROR', error});
        }
    }
};


export const logInGoogle = (data) => {
    return async (dispatch) => {
        try {
            const response = await axios.post(`${process.env.REACT_APP_API_URL}/socialslogin`, data);
            let logInData = {
                user: response.data.data
            };
            dispatch({type: 'LOGIN_SUCCESS', logInData});
            return <Redirect to='/login' />;
        } catch (error) {
            console.log(error.message);
            dispatch({ type: 'LOGIN_ERROR', error });
        }

    }
};


export const logOut = () => {
    return async (dispatch, getState) => {
        try {
            const getstate = getState();
            const data = {
                id:getstate.auth.user._id
            }
            const response = await axios.post(`${process.env.REACT_APP_API_URL}/logout`,data);
            if(!response.data.data) {
                dispatch({type: 'LOGOUT_ERROR', });
                return;
            }
            dispatch({type: 'LOGOUT_SUCCESS'});
        } catch (error) {
            console.log(error.message);
            dispatch({ type: 'LOGIN_ERROR', error });
        }

    }
};

export const getCookie = (cookies) => {
    return async (dispatch) => {
        try {
            dispatch({type: 'GET_COOKIES', cookies});
        } catch (error) {
            console.log(error.message);
            dispatch({ type: 'GET_COOKIES_ERROR', error });
        }

    }
};

export const getUserName = async (iduser) => {
        try {
            const response = await axios.get(`${process.env.REACT_APP_API_URL}/getuser/${iduser}`);
            console.log('response',response);
            return response.data.data.displayName
        } catch (error) {
            console.log(error.message);
        }
};

export const getUser = (iduser) => {
    return async (dispatch) => {
        try {
            const response = await axios.get(`${process.env.REACT_APP_API_URL}/getuser/${iduser}`);
            let logInData = {
                user: response.data.data,
            };
            dispatch({type: 'GET_USER_SUCCESS', logInData});
        } catch (error) {
            console.log(error.message);
            dispatch({type: 'GET_USER_ERROR', error});
        }
    }
};

export const getUserLikes = () => {
    return async (dispatch, getState) => {
        try {
            const getstate = getState();
            const response = await axios.get(`${process.env.REACT_APP_API_URL}/likes/${getstate.auth.user._id}`);
            let userLikedMeetups = response.data.data.map(like => like._meetup);
            dispatch({type: 'GET_USER_LIKES', userLikedMeetups});
        } catch (error) {
            console.log(error);
        }

    }
};

export const likeMeetup = (meetup_id) => {
    return async (dispatch, getState) => {
        try {
            const getstate = getState();
            const data = {
                meetup_id: meetup_id,
                user_id:getstate.auth.user._id
            }
            const { auth } = getState();
            await axios.post(`${process.env.REACT_APP_API_URL}/like`, data);
            let likedMeetups = auth.userLikedMeetups;
            let meetupExists = likedMeetups.includes(meetup_id);
            const addLike = likedMeetups.concat(meetup_id);
            const deleteLike = likedMeetups.filter(meetup => meetup !== meetup_id);
            let userLikedMeetups = meetupExists ? deleteLike : addLike;
            dispatch({type: 'LIKE_MEETUP', userLikedMeetups});
        } catch (error) {
            console.log(error);
            dispatch({type: 'LIKE_MEETUP_ERROR', error});
        }
    }
};









