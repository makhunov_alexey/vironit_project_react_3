import axios from "axios";

export const createMeetup = (credentials) => {
    return async (dispatch, getState) => {
        try {
            const getstate = getState();
            credentials._id = getstate.auth.user._id;
            const response = await axios.post(`${process.env.REACT_APP_API_URL}/createmeetup`, credentials);
            dispatch({type: 'CREATE_MEETUP', response});
        } catch (error) {
            console.log(error);
            dispatch({type: 'CREATE_MEETUP_ERROR', error});
        }
    }
}

export const getMeetupList = () => {
    return async (dispatch) => {
        try {
            const response = await axios.get(`${process.env.REACT_APP_API_URL}/showmeetups`);
            const meetupList = response.data.data;
            dispatch({type: 'GET_MEETUP_LIST', meetupList});
        } catch (error) {
            dispatch({type: 'GET_MEETUP_LIST_ERROR', error});
        }
    }
};

export const updateMeetup = (Update) => {
    return async (dispatch) => {
        try {
            const response = await axios.put(`${process.env.REACT_APP_API_URL}/changeMeetup`, Update.newData);
            dispatch({type: 'UPDATE_MEETUP_DESCRIPTION', response});
        } catch (error) {
            dispatch({type: 'UPDATE_MEETUP_DESCRIPTION_ERROR', error});
        }
    }
};

export const deleteMeetup = (dataToDelete) => {
    return async (dispatch,getState) => {
        try {
            const getstate = getState();
            await axios.delete(`${process.env.REACT_APP_API_URL}/deleteMeetup/${dataToDelete.meetup_id}/${getstate.auth.user._id}`);
            dispatch({type: 'DELETE_MEETUP_DESCRIPTION'});
        } catch (error) {
            console.log(error);
            dispatch({type: 'UPDATE_MEETUP_DESCRIPTION_ERROR', error});
        }
    }
};