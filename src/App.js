import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import SignUp from './components/auth/SignUp';
import LognIn from './components/auth/LognIn';
import CreateMeetup from './components/meetup/CreateMeetup';
import MeetupView from './components/meetup/MeetupView';
import Calendar from './components/calendar/Calendar'
import {instanceOf} from 'prop-types';
import {withCookies, Cookies} from 'react-cookie';
import connect from "react-redux/es/connect/connect";
import {getCookie} from "./store/actions/authActions";
import {getUser} from "./store/actions/authActions";
import AllMeetups from './components/meetup/AllMeetups';


class App extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);
        const {cookies} = props;
        this.state = {
            isAuthenticated: false,
            user: null, token: '',
            cookies: cookies,
            meetups:[]
        };
        this.props.getCookie(cookies)
    }

    componentDidMount() {
        const {cookies} = this.props;
    }

    render() {
        return (
            <BrowserRouter>
                <div className="meetup-app">
                <Navbar cookies={this.props.cookies}/>
                    <Switch>
                    <Route path='/login' render={() => (<LognIn cookies={this.props.cookies}/>)}/>
                    <Route path='/signup' component={SignUp}/>
                    <Route path='/createmeetup' component={CreateMeetup}/>
                    <Route path='/meetup/:meetup_id' component={MeetupView}/>
                    <Route path='/calendar' component={Calendar}/>
                    <Route exact path='/' component={AllMeetups}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCookie: (cookie) => dispatch(getCookie(cookie)),
        getUser: (token) => dispatch(getUser(token))
    }
};

export default connect(null, mapDispatchToProps)(withCookies(App));
